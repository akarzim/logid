#!/usr/bin/env ruby
# frozen_string_literal: true

require "bundler/setup"
require "dry/cli"
require "zip/filesystem"

module Logid
  module CLI
    module Commands
      extend Dry::CLI::Registry

      class Top < Dry::CLI::Command
        desc "Print top IDs from a log zip (default: 5)"

        argument :zipfilepath, desc: "zipfile path"
        argument :limit, default: 5, required: false, desc: "top limit"

        def call(zipfilepath:, limit:, **)
          extractor = IdsExtractor.new(zipfilepath)
          extracted_ids = extractor.call

          top_ids = TopIds.new(extracted_ids)
          puts top_ids.call(limit: limit.to_i)
        end
      end

      register "top", Top, aliases: ["t"]
    end
  end

  # Extract IDs from URLs inside zipped logfiles
  class IdsExtractor
    LOG_REGEX = /\Alogs\/.*\.log\z/.freeze

    def initialize(zipfilepath)
      @zipfilepath = zipfilepath
      @extracted_ids = []
    end

    # Return the IDs extracted from URLs params
    #
    # @return [Array] the extracted IDs
    def call
      Zip::File.open(zipfilepath) do |zipfile|
        zipfile.each do |entry|
          next unless entry.name =~ LOG_REGEX

          content = zipfile.file.readlines(entry.name)

          ids = content.map do |line|
            line.match(/id=(\w+)/).captures
          end

          @extracted_ids += ids.flatten
        end
      end

      extracted_ids
    end

    private

    attr_reader :zipfilepath, :extracted_ids
  end

  # Grab the most frequent IDs from an array
  class TopIds
    def initialize(ids_array)
      @ids_array = ids_array
    end

    # Return the most frequent IDs
    #
    # @param limit [Integer] the number of IDs to return
    # @return [Array] the most frequent IDs
    def call(limit: 5)
      ids_array.
        tally.
        sort_by { |_k, v| v }.
        last(limit).
        reverse.
        to_h.
        keys
    end

    private

    attr_reader :ids_array, :limit
  end
end

Dry::CLI.new(Logid::CLI::Commands).call
