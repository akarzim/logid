# Logid

## Contexte

Ceci est un exercice qui servira de base de discussion à un entretien technique.

## Consignes

Dans le fichier logs.zip ci-joint, tu trouveras des fichiers avec URLs. La plupart ont un paramètre id.

Nous souhaitons savoir quels sont les 5 ids que l'on retrouve dans le plus d’URLs.

Pourrais-tu nous envoyer :

- les 5 ids les plus fréquents,
- le code qui t'a permis d’arriver au résultat.

## Postulats

J'ai fait le postulat que le format ZIP n'était pas qu'une commodité d'envoi,
mais bien le format de fichier à manipuler.

Je suis aussi parti du postulat que l'exercice est attendu en Ruby. Cependant,
ma première idée fut de répondre à cette demande à l'aide de quelques outils en
ligne de commande :

```sh
zipgrep -o 'id=[0-9]+' data/logs.zip | awk -F '=' '{print $2}' | sort | uniq -c | sort -r | head -n 5 | awk '{print $2}'
```

## Prérequis

Ruby 3.2.2

## Installation

```
bundle install
```

## Usage

```
Commands:
  logid.rb top [ZIPFILEPATH] [LIMIT]                   # Print top IDs from a log zip (default: 5)
```

```
Command:
  logid.rb top

Usage:
  logid.rb top [ZIPFILEPATH] [LIMIT]

Description:
  Print top 5 IDs from a log zip

Arguments:
  ZIPFILEPATH                       # zipfile path
  LIMIT                             # top limit
```

### Exemple

```
❯ bundle exec logid.rb top data/logs.zip
413
505
397
473
512
```
